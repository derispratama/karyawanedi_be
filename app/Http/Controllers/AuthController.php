<?php

namespace App\Http\Controllers;

use App\Http\Resources\KaryawanResource;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $user = User::where('email', $request->email)->first();

        if (!$user || !Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'email' => ['email tidak ditemukan']
            ]);
        }

        return response()->json([
            'token' => $user->createToken('user login')->plainTextToken,
            'user' => new UserResource($user->loadMissing('karyawan'))
        ]);
    }

    public function register(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        $request['role'] = 'USER';
        $user = User::create($request->all());
        return response()->json([
            'token' => $user->createToken('user login')->plainTextToken,
            'user' => new KaryawanResource($user),
        ]);
    }

    public function logout(Request $request)
    {

        return response()->json(['ok' => 'ok']);
    }
}
