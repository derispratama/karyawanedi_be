<?php

namespace App\Http\Controllers;

use App\Http\Resources\KaryawanResource;
use App\Models\Karyawan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class KaryawanController extends Controller
{
    public function index()
    {
        $karyawan = Karyawan::with('jenjang_pendidikan')->with('riwayat_pelatihan')->with('riwayat_pekerjaan')->get();
        return KaryawanResource::collection($karyawan);
    }

    public function show($id)
    {
        $karyawan = Karyawan::with('jenjang_pendidikan')->with('riwayat_pelatihan')->with('riwayat_pekerjaan')->findOrFail($id);
        return new KaryawanResource($karyawan);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'posisi' => 'required',
            'tempat' => 'required',
            'ktp' => 'required',
            'tgl_lahir' => 'required',
            'jk' => 'required',
            'agama' => 'required',
            'gol_darah' => 'required',
            'status' => 'required',
            'alamat_tinggal' => 'required',
            'alamat_ktp' => 'required',
            'telp' => 'required',
            'org_dekat' => 'required',
            'skill' => 'required',
            'bersedia' => 'required',
            'penghasial_diharapkan' => 'required',
            'email' => 'required',
        ]);



        $data = [];
        $data['id_user'] = $request->id_user;
        $data['name'] = $request->name;
        $data['tempat'] = $request->tempat;
        $data['posisi'] = $request->posisi;
        $data['ktp'] = $request->ktp;
        $data['tgl_lahir'] = $request->tgl_lahir;
        $data['jk'] = $request->jk;
        $data['agama'] = $request->agama;
        $data['gol_darah'] = $request->gol_darah;
        $data['status'] = $request->status;
        $data['alamat_tinggal'] = $request->alamat_tinggal;
        $data['alamat_ktp'] = $request->alamat_ktp;
        $data['telp'] = $request->telp;
        $data['org_dekat'] = $request->org_dekat;
        $data['skill'] = $request->skill;
        $data['bersedia'] = $request->bersedia;
        $data['penghasial_diharapkan'] = $request->penghasial_diharapkan;
        $data['email'] = $request->email;

        $karyawan = Karyawan::create($data);
        for ($i = 0; $i < count($request->jenjang_pendidikan['name']); $i++) {
            $datapd['id_user'] = $karyawan->id;
            $datapd['name'] = $request->jenjang_pendidikan['name'][$i];
            $datapd['nama_institusi'] = $request->jenjang_pendidikan['nama_institusi'][$i];
            $datapd['jurusan'] = $request->jenjang_pendidikan['jurusan'][$i];
            $datapd['tahun_lulus'] = $request->jenjang_pendidikan['tahun_lulus'][$i];
            $datapd['ipk'] = $request->jenjang_pendidikan['ipk'][$i];

            DB::table('jenjang_pendidikans')->insert($datapd);
        }

        for ($i = 0; $i < count($request->riwayat_pelatihan['name']); $i++) {
            $datapl['id_user'] = $karyawan->id;
            $datapl['name'] = $request->riwayat_pelatihan['name'][$i];
            $datapl['sertifikat'] = $request->riwayat_pelatihan['sertifikat'][$i];
            $datapl['tahun'] = $request->riwayat_pelatihan['tahun'][$i];

            DB::table('riwayat_pelatihans')->insert($datapl);
        }

        for ($i = 0; $i < count($request->riwayat_pekerjaan['name']); $i++) {
            $datapj['id_user'] = $karyawan->id;
            $datapj['name'] = $request->riwayat_pekerjaan['name'][$i];
            $datapj['posisi_terakhir'] = $request->riwayat_pekerjaan['posisi_terakhir'][$i];
            $datapj['pendapatan_terakhir'] = $request->riwayat_pekerjaan['pendapatan_terakhir'][$i];
            $datapj['tahun'] = $request->riwayat_pekerjaan['tahun'][$i];

            DB::table('riwayat_pekerjaans')->insert($datapj);
        }

        return new KaryawanResource($karyawan);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'posisi' => 'required',
            'tempat' => 'required',
            'ktp' => 'required',
            'tgl_lahir' => 'required',
            'jk' => 'required',
            'agama' => 'required',
            'gol_darah' => 'required',
            'status' => 'required',
            'alamat_tinggal' => 'required',
            'alamat_ktp' => 'required',
            'telp' => 'required',
            'org_dekat' => 'required',
            'skill' => 'required',
            'bersedia' => 'required',
            'penghasial_diharapkan' => 'required',
            'email' => 'required',
        ]);



        $data = [];
        $data['name'] = $request->name;
        $data['tempat'] = $request->tempat;
        $data['posisi'] = $request->posisi;
        $data['ktp'] = $request->ktp;
        $data['tgl_lahir'] = $request->tgl_lahir;
        $data['jk'] = $request->jk;
        $data['agama'] = $request->agama;
        $data['gol_darah'] = $request->gol_darah;
        $data['status'] = $request->status;
        $data['alamat_tinggal'] = $request->alamat_tinggal;
        $data['alamat_ktp'] = $request->alamat_ktp;
        $data['telp'] = $request->telp;
        $data['org_dekat'] = $request->org_dekat;
        $data['skill'] = $request->skill;
        $data['bersedia'] = $request->bersedia;
        $data['penghasial_diharapkan'] = $request->penghasial_diharapkan;
        $data['email'] = $request->email;

        $karyawan = Karyawan::findOrFail($id);
        $karyawan->update($data);

        for ($i = 0; $i < count($request->jenjang_pendidikan['name']); $i++) {
            $datapd['name'] = $request->jenjang_pendidikan['name'][$i];
            $datapd['nama_institusi'] = $request->jenjang_pendidikan['nama_institusi'][$i];
            $datapd['jurusan'] = $request->jenjang_pendidikan['jurusan'][$i];
            $datapd['tahun_lulus'] = $request->jenjang_pendidikan['tahun_lulus'][$i];
            $datapd['ipk'] = $request->jenjang_pendidikan['ipk'][$i];
            if (isset($request->jenjang_pendidikan['id'][$i]) && $request->jenjang_pendidikan['id'][$i]) {
                DB::table('jenjang_pendidikans')->where('id', $request->jenjang_pendidikan['id'])->update($datapd);
            } else {
                $datapd['id_user'] = $karyawan->id;
                DB::table('jenjang_pendidikans')->insert($datapd);
            }
        }

        for ($i = 0; $i < count($request->riwayat_pelatihan['name']); $i++) {
            $datapl['name'] = $request->riwayat_pelatihan['name'][$i];
            $datapl['sertifikat'] = $request->riwayat_pelatihan['sertifikat'][$i];
            $datapl['tahun'] = $request->riwayat_pelatihan['tahun'][$i];

            if (isset($request->jenjang_pendidikan['id'][$i]) && $request->riwayat_pelatihan['id'][$i]) {
                DB::table('riwayat_pelatihans')->where('id', $request->riwayat_pelatihan['id'])->update($datapl);
            } else {
                $datapl['id_user'] = $karyawan->id;
                DB::table('riwayat_pelatihans')->insert($datapl);
            }
        }

        for ($i = 0; $i < count($request->riwayat_pekerjaan['name']); $i++) {
            $datapj['name'] = $request->riwayat_pekerjaan['name'][$i];
            $datapj['posisi_terakhir'] = $request->riwayat_pekerjaan['posisi_terakhir'][$i];
            $datapj['pendapatan_terakhir'] = $request->riwayat_pekerjaan['pendapatan_terakhir'][$i];
            $datapj['tahun'] = $request->riwayat_pekerjaan['tahun'][$i];

            if (isset($request->jenjang_pendidikan['id'][$i]) && $request->riwayat_pekerjaan['id'][$i]) {
                DB::table('riwayat_pekerjaans')->where('id', $request->riwayat_pekerjaan['id'])->update($datapj);
            } else {
                $datapj['id_user'] = $karyawan->id;
                DB::table('riwayat_pekerjaans')->insert($datapj);
            }
        }

        return new KaryawanResource($karyawan);
    }

    public function destroy($id)
    {
        DB::table('jenjang_pendidikans')->where('id_user', $id)->delete();
        DB::table('riwayat_pekerjaans')->where('id_user', $id)->delete();
        DB::table('riwayat_pelatihans')->where('id_user', $id)->delete();

        $karyawan = Karyawan::findOrFail($id);
        $karyawan->delete();
        return new KaryawanResource($karyawan);
    }
}
