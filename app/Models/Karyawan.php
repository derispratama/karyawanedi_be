<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Karyawan extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function jenjang_pendidikan(): HasMany
    {
        return $this->hasMany(JenjangPendidikan::class, 'id_user', 'id');
    }

    public function riwayat_pelatihan(): HasMany
    {
        return $this->hasMany(RiwayatPelatihan::class, 'id_user', 'id');
    }

    public function riwayat_pekerjaan(): HasMany
    {
        return $this->hasMany(RiwayatPekerjaan::class, 'id_user', 'id');
    }
}
